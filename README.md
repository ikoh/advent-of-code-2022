# Advent of Code 2022

Structure for multiple executables in one Cabal project.

Folder structure:

```
. <your-project>
|-- package.yaml
|-- ex-01
|     |-- Main.hs
|-- ex-02
      |-- Main.hs
```

In Cabal:

```
executables:
    ex-01:
        main: Main.hs
        source-dirs: ex-01
    ex-02:
        main: Main.hs
        source-dirs: ex-02
```

See the full discussion [here](https://www.reddit.com/r/haskell/comments/capuz7/multiple_executable_in_project/). See [here](https://stackoverflow.com/questions/60982799/how-can-i-configure-cabal-to-build-two-executables) to build specific executables in a Cabal project with multiple executables.

In terms of **getting HLS to work**, see [this thread](https://discourse.haskell.org/t/resolved-hls-works-inconsistently-or-not-at-all-inside-cabal/8162/4). Note that, because the executables have unique names, `hie.yaml` could also be specified like this:

```yaml
cradle:
  cabal:
    - path: "day-01/Main.hs"
      component: "day-01"

    - path: "day-02/Main.hs"
      component: "day-02"

    - path: "day-03/Main.hs"
      component: "day-03"
```

**Regardless, whenever a new `hie.yaml` file is created, always remember to restart HLS in VS Code!**