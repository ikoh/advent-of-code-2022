module Main where

import qualified Data.Map.Strict as Map (fromList, lookup, Map)
import qualified Data.Set as Set (fromList, toList)
import Data.List (sort)
import Data.List.Split (chunksOf)
import Data.Maybe (fromMaybe)

type Compartment1 = String
type Compartment2 = String
type CommonItem   = Char
type Priority     = Integer

main :: IO ()
main = do
    raw <- readFile "./data/day_03.txt"

    let parsed = lines raw :: [String]

        -- Part 1
        lookupList :: Map.Map CommonItem Priority
        lookupList = Map.fromList $ zip ['a'..'z'] [1..] ++ zip ['A'..'Z'] [27..]
        commonItems = map (itemInCommon . divideAndDistinct) parsed :: [CommonItem]
        commonItemPriorities = map (getPriority lookupList) commonItems :: [Maybe Integer]

        -- Part 2
        parsedTriplets = chunksOf 3 parsed :: [[String]]

    -- Solve part 1 (7428)
    print . sum $ map (fromMaybe 0) commonItemPriorities

divideAndDistinct :: String -> (Compartment1, Compartment2)
divideAndDistinct items = (
    sort . distinct $ take compartmentSize items,
    sort . distinct $ drop compartmentSize items
    )
  where
    compartmentSize :: Int
    compartmentSize = round $ (fromIntegral $ length items :: Double) / 2

    distinct :: String -> String
    distinct = Set.toList . Set.fromList

itemInCommon :: (Compartment1, Compartment2) -> CommonItem
itemInCommon (comp1, comp2) = head . map fst . filter snd $ map (itemTest comp2) comp1
  where
    itemTest :: Compartment2 -> Char -> (Char, Bool)
    itemTest comp2 x = (x, x `elem` comp2)

getPriority :: Ord k => Map.Map k a -> k -> Maybe a
getPriority lookupList commonItem = Map.lookup commonItem lookupList