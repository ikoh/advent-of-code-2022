module Main where

import Data.List (sort)
import Data.List.Split (splitOn)

main :: IO ()
main = do
    raw <- readFile "./data/day_01.txt"

    let caloriesPerElf = map sum $ parse raw

    -- Part 1 answer
    print $ maximum caloriesPerElf

    -- Part 2 answer
    print . sum . take 3 . reverse $ sort caloriesPerElf

parse :: String -> [[Int]]
parse input = secondPass $ firstPass input
  where
    firstPass :: String -> [String]
    firstPass = splitOn "\n\n"

    secondPass :: [String] -> [[Int]]
    secondPass = map (map read . splitOn "\n")