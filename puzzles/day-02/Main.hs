module Main where

main :: IO ()
main = do
    raw <- readFile "./data/day_02.txt"
    let preprocessedInput = map words $ lines raw :: [[String]]
        parsedPart1 = map parsePart1 preprocessedInput :: [(String, Int)]
        parsedPart2 = map parsePart2 preprocessedInput
    -- Part 1 output (14375)
    print . sum $ map getScore parsedPart1
    -- Part 2 output (10274)
    print . sum $ map getFinalScoreCheating parsedPart2

parsePart1 :: [String] -> (String, Int)
parsePart1 x = (head x, xyzTo123 x)
  where
    xyzTo123 :: [String] -> Int
    xyzTo123 x
        | last x == "X" = 1
        | last x == "Y" = 2
        | otherwise     = 3

getScore :: (String, Int) -> Int
getScore (opponent, player) = case (opponent, player) of
    ("B", 1) -> 1 + 0
    ("C", 1) -> 1 + 6
    ("A", 2) -> 2 + 6
    ("C", 2) -> 2 + 0
    ("A", 3) -> 3 + 0
    ("B", 3) -> 3 + 6
    (_, x)   -> x + 3

getGestureScore :: String -> Int
getGestureScore gesture = case gesture of
    "A" -> 1  -- Rock
    "B" -> 2  -- Paper
    "C" -> 3  -- Scissors

getOutcomeScore :: String -> Int
getOutcomeScore outcome = case outcome of
    "X" -> 0  -- Lose
    "Y" -> 3  -- Draw
    "Z" -> 6  -- Win

parsePart2 :: [String] -> (String, String)
parsePart2 x = (head x, last x)

getFinalScoreCheating :: (String, String) -> Int
getFinalScoreCheating (gesture, outcome) = case (gesture, outcome) of
    ("A", "X") -> getOutcomeScore "X" + getGestureScore "C"
    ("A", "Y") -> getOutcomeScore "Y" + getGestureScore "A"
    ("A", "Z") -> getOutcomeScore "Z" + getGestureScore "B"
    ("B", "X") -> getOutcomeScore "X" + getGestureScore "A"
    ("B", "Y") -> getOutcomeScore "Y" + getGestureScore "B"
    ("B", "Z") -> getOutcomeScore "Z" + getGestureScore "C"
    ("C", "X") -> getOutcomeScore "X" + getGestureScore "B"
    ("C", "Y") -> getOutcomeScore "Y" + getGestureScore "C"
    _          -> getOutcomeScore "Z" + getGestureScore "A"